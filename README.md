# CarCar

## Team
* Francisco - Sales
* Services coming soon

## How to run the project
## Step-by-Step Instructions to Run the Project

### Pre-requisites
- Docker
- Node.js and npm
- Git

### Setup
1. **Clone the Repository**: Clone the project repository to your local machine.
    ```bash
    git clone https://gitlab.com/sjp19-public-resources/sjp-2022-april/project-beta.git
    ```

2. **Navigate to Project Directory**:
    ```bash
    cd project-beta
    ```

3. **Create Docker Volume**: Create a Docker volume to persist data.
    ```bash
    docker volume create beta-data
    ```

4. **Build Docker Containers**: Build all services defined in `docker-compose.yml`.
    ```bash
    docker-compose build
    ```

5. **Run Docker Containers**: Start all services.
    ```bash
    docker-compose up
    ```

### Initial Configuration
1. **Check Services**: Make sure all microservices are up and running by navigating to their respective ports via a web browser.

### Using the Application
1. **Frontend**: Navigate to `http://localhost:3000/` to interact with the application.
2. **APIs**: Use Insomnia interact with the  APIs. The endpoints are documented in the API section of this README.

### Stopping the Services
1. **Stop Docker Containers**: Stop all services without removing containers.
    ```bash
    docker-compose stop
    ```

2. **Remove Docker Containers**: To stop and remove all containers.
    ```bash
    docker-compose down
    ```



## Design
The project is structured using a MERN stack:
- **MongoDB**
- **Express.js**
- **React**
- **Node.js**

The front-end communicates with the back-end through RESTful API endpoints, fetching data as needed and sending data for CRUD operations.

## Service Microservice
Coming soon

## Sales Microservice

The Sales microservice is responsible for managing all operations related to sales transactions, salespeople, and customers. This service is designed to be flexible, scalable, and integrated seamlessly with the Inventory microservice.

### Models

#### Salesperson Model
- **Fields**: `first_name`, `last_name`, `employee_id`
- **Description**: This model is responsible for storing information about salespeople.

#### Customer Model
- **Fields**: `first_name`, `last_name`, `address`, `phone_number`
- **Description**: This model holds all the necessary details about a customer involved in a sale.

#### Sale Model
- **Fields**: `automobile`, `salesperson`, `customer`, `price`
- **Description**: This model captures the sales transactions. It has foreign keys linking it to the Salesperson, Customer, and Automobile models.

#### AutomobileVO Model
- **Fields**: `vin`, `sold`
- **Description**: This is a simplified version of the Automobile model, capturing only the Vehicle Identification Number (VIN) and whether it is sold.

### Integration with Inventory Microservice
The Sales microservice is tightly integrated with the Inventory microservice. Here's how:
1. **Data Fetching**: The Sales microservice fetches unsold automobiles from the Inventory microservice to display as options for new sales transactions.
2. **Real-time Updates**: When a sale occurs, the Sales microservice updates the `sold` status in the Inventory microservice.
3. **Polling**: An automobile poller runs every 60 seconds, updating the list of automobiles from the Inventory microservice.
4. **Error Handling**: In case of discrepancies or errors, appropriate error codes are returned, and both services roll back to maintain data integrity.

## API Endpoints

### Salespeople

---

#### **List All Salespeople**
  - **Method**: `GET`
  - **URL**: `http://localhost:8090/api/salespeople/`

#### **Create a Salesperson**
  - **Method**: `POST`
  - **URL**: `http://localhost:8090/api/salespeople/`

#### **Delete a Salesperson**
  - **Method**: `DELETE`
  - **URL**: `http://localhost:8090/api/salespeople/:id`

---

### Customers

---

#### **List All Customers**
  - **Method**: `GET`
  - **URL**: `http://localhost:8090/api/customers/`

#### **Create a Customer**
  - **Method**: `POST`
  - **URL**: `http://localhost:8090/api/customers/`

#### **Delete a Customer**
  - **Method**: `DELETE`
  - **URL**: `http://localhost:8090/api/customers/:id`

---

### Sales

---

#### **List All Sales**
  - **Method**: `GET`
  - **URL**: `http://localhost:8090/api/sales/`

#### **Create a Sale**
  - **Method**: `POST`
  - **URL**: `http://localhost:8090/api/sales/`

#### **Delete a Sale**
  - **Method**: `DELETE`
  - **URL**: `http://localhost:8090/api/sales/:id`

---

## API Endpoints

In this section, you will find detailed examples and payload structures for each API endpoint.

---

### Salespeople

---

#### **List All Salespeople**

- **Method**: `GET`
- **URL**: `http://localhost:8090/api/salespeople/`

**Example Response**
```json
{
	"sales": [
		{
			"price": 41155874.0,
			"automobile": {
				"vin": "1C4BJWKGXDL508578",
				"sold": true,
				"id": 11
			},
        }
    ]
```

#### **Create a Salesperson**

- **Method**: `POST`
- **URL**: `http://localhost:8090/api/salespeople/`

**Example POST**
```json
		{
			"color": "black",
			"year": 1991,
			"vin": "JTLKT324764045539",
			"sold': false,
			"model": 1
		}
```
---

### Customers

---

#### **List All Customers**

- **Method**: `GET`
- **URL**: `http://localhost:8090/api/customers/`

**Example Response**
```json
{
	"customers": [
		{
			"first_name": "Nely",
			"last_name": "Escobar",
			"address": "8336 grand pacific dr",
			"phone_number": "702-555-669",
			"id": 1
		},
    ]
}
```

#### **Create a Customers**

- **Method**: `POST`
- **URL**: `http://localhost:8090/api/customers/`

**Example POST**
```json
		{
			"first_name": "Francisco",
			"last_name": "Escobar",
			"address": "8336 grand pacific dr",
			"phone_number": "702-555-669"
		}
```
---

### Sales

---

#### **List All Sales**

- **Method**: `GET`
- **URL**: `http://localhost:8090/api/sales/`

**Example Response**
```json
{
	"sales": [
		{
			"price": 41155874.0,
			"automobile": {
				"vin": "1C4BJWKGXDL508578",
				"sold": true,
				"id": 11
			},
    ]
}
```

#### **Create a Sale**

- **Method**: `POST`
- **URL**: `http://localhost:8090/api/sales/`

**Example POST**
```json
{
  "price": 40000.0,
  "automobile": "5LMJJ2J57CEL03980",
  "sales_person": 1,
  "customer": 1
}

```
---

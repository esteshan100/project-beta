from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.core.exceptions import ObjectDoesNotExist
from .models import AutomobileVO, Salesperson, Customer, Sale

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "id"]
    encoders = {}

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_number", "id"]
    encoders = {}

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]
    encoders = {}

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["price","automobile", "sales_person", "customer", "id"]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }

# Sales people views
@require_http_methods(["GET","POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse({"salespeople": salespeople}, encoder=SalespersonEncoder)
        except Salesperson.DoesNotExist:
            return JsonResponse({"Error": "No sales person"}, status=404)
    else:
        try:
            content = json.loads(request.body)
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(salespeople, encoder=SalespersonEncoder, safe=False)
        except Salesperson.DoesNotExist:
            return JsonResponse({"Error": "No sales person"}, status=404)


@require_http_methods(["DELETE"])
def api_delete_salesperson(request,id):
    count, _ = Salesperson.objects.filter(id=id).delete()
    if count == 0:
        return JsonResponse({"Error": "No Salesperson with that id exists"}, status=404)

    return JsonResponse({"deleted": count}, safe=False)


@require_http_methods(["GET","POST"])
def api_list_customer(request):
    if request.method == "GET":
        try:
            customers = Customer.objects.all()
            return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
        except Customer.DoesNotExist:
            return JsonResponse({"Error": "No customer"}, status=404)
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(customers, encoder=CustomerEncoder, safe=False)
        except Customer.DoesNotExist:
            return JsonResponse({"Error": "No customer"}, status=404)

@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
    count, _ = Customer.objects.filter(id=id).delete()

    if count == 0:
        return JsonResponse({"Error": "No customer with that id exists"}, status=404)

    return JsonResponse({"deleted": count}, safe=False)




# Sales views
@require_http_methods(["GET","POST"])
def api_list_sale(request):
    if request.method == "GET":
        try:
            sales = Sale.objects.all()
            return JsonResponse({"sales": sales}, encoder=SaleEncoder)
        except Sale.DoesNotExist:
            return JsonResponse({"Error": "No sale"}, status=404)
    else:
            try:
                content = json.loads(request.body)

                # Fetch instances for the foreign keys
                try:
                    automobile_vin = content['automobile']
                    automobile = AutomobileVO.objects.get(vin=automobile_vin)
                    sales_person = Salesperson.objects.get(id=content.get('sales_person'))
                    customer = Customer.objects.get(id=content.get('customer'))
                except ObjectDoesNotExist:
                    return JsonResponse({"Error": "One of the related objects does not exist"}, status=400)

                # Create the Sale instance
                sale = Sale.objects.create(
                    price=content.get('price'),
                    automobile=automobile,
                    sales_person=sales_person,
                    customer=customer
                )
                return JsonResponse(sale, encoder=SaleEncoder, safe=False)
            except Sale.DoesNotExist:
                return JsonResponse({"Error": "Could not create sale"}, status=400)




@require_http_methods(["DELETE","PUT"])
def api_delete_sale(request, id):
    if request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        if count == 0:
            return JsonResponse({"Error": "No Sales was made with that id"}, status=404)
        return JsonResponse({"deleted": count}, safe=False)

    elif request.method == "PUT":
        try:
            # Parse incoming JSON payload
            content = json.loads(request.body)

            # Fetch the Sale object to update
            sale = Sale.objects.get(id=id)

            # Update the Sale fields
            sale.price = content.get('price', sale.price)

            # Fetch instances for the foreign keys if they exist in the payload
            if 'automobile' in content and 'vin' in content['automobile']:
                sale.automobile = AutomobileVO.objects.get(vin=content['automobile']['vin'])
            if 'sales_person' in content:
                sale.sales_person = Salesperson.objects.get(id=content.get('sales_person'))
            if 'customer' in content:
                sale.customer = Customer.objects.get(id=content.get('customer'))

            # Save the changes
            sale.save()

            return JsonResponse(sale, encoder=SaleEncoder, safe=False)
        except Sale.DoesNotExist:
            return JsonResponse({"Error": "Sale with that id does not exist"}, status=404)
        except ObjectDoesNotExist:
            return JsonResponse({"Error": "One of the related objects does not exist"}, status=400)


# Automobile views
@require_http_methods(["GET"])
def api_show_automobile(request):
    try:
        automobile = AutomobileVO.objects.filter(sold=False)
        return JsonResponse({"automobile": automobile}, encoder=AutomobileVOEncoder)
    except AutomobileVO.DoesNotExist:
        return JsonResponse({"Error": "No automobile"}, status=404)

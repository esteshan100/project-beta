import { useEffect, useState } from 'react';

// ShowAutomobiles Component: Responsible for displaying a list of automobiles.
function ShowAutomobiles() {

  // State Initialization: Storing the list of automobiles fetched from the API.
  const [autos, setAutos] = useState([]);

  // Function to Fetch Automobile Data from the API.
  const getData = async () => {
    try {
      // Making the API call.
      const response = await fetch('http://localhost:8100/api/automobiles/');

      // Handling successful API call.
      if (response.ok) {
        // De-structure 'autos' from the API response and update the state.
        const { autos } = await response.json();
        setAutos(autos);
      } else {
        // Logging server errors.
        console.error('Server Error:', response);
      }
    } catch (error) {
      // Logging network errors.
      console.error('Network Error:', error);
    }
  };

  // useEffect Hook: Triggered when the component mounts to fetch data.
  useEffect(() => {
    getData();
  }, []);

  // JSX Rendering.
  return (
    <div>
      <h1>Automobiles</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Model</th>
            <th>Year</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {
            // Loop to Render Each Automobile as a Row in the Table.
            autos.map(auto => (
              <tr key={auto.id}>
                <td>{auto.vin}</td>
                <td>{auto.color}</td>
                <td>{auto.model.name}</td>
                <td>{auto.year}</td>
                <td>{auto.model.manufacturer.name}</td>
                {/* wont display true of false with out this code */}
                <td>{auto.sold ? 'Sold' : 'Available'}</td>
              </tr>
            ))
          }
        </tbody>
      </table>
    </div>
  );
}

export default ShowAutomobiles;

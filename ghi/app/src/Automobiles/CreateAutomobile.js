import React, { useState, useEffect } from 'react';

// CreateAutomobile component is responsible for creating new automobiles.
function CreateAutomobile() {

  // State variable to store the list of vehicle models fetched from the API.
  const [models, setModels] = useState([]);

  // State variable to manage form data.
  const [formData, setFormData] = useState({
    color: '',
    year: '',
    vin: '',
    model_id: '',
  });

  // Fetch the list of vehicle models from the API.
  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/models/');
      if (response.ok) {
        const { models } = await response.json();
        setModels(models);
      } else {
        console.error('Server Error:', response);
      }
    } catch (error) {
      console.error('Network Error:', error);
    }
  };

  // Fetch the list of models initially when the component mounts.
  useEffect(() => {
    getData();
  }, []);

  // Handle form submission to create a new automobile.
  const handleSubmit = async (event) => {
    event.preventDefault();

    // URL and configurations for POST request to create an automobile.
    const apiUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    // Make the POST request with form data.
    const response = await fetch(apiUrl, fetchConfig);

    // Reset the form data to initial state if the API call is successful.
    if (response.ok) {
      setFormData({
        color: '',
        year: '',
        vin: '',
        model_id: '',
      });
    }
  };

  // Handle changes to form inputs.
  const handleFormChange = (e) => {
    const value = e.target.value;  // Extract value from the changed input.
    const inputName = e.target.name;  // Extract input name.

    // Update formData state with new input value.
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an Automobile</h1>
          <form onSubmit={handleSubmit}>
            {/* Input field for Color */}
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            {/* Input field for Year */}
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.year} placeholder="year" required type="number" name="year" id="year" className="form-control" />
              <label htmlFor="year">Year</label>
            </div>
            {/* Input field for VIN */}
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
              <label htmlFor="vin">VIN</label>
            </div>
            {/* Dropdown to select Vehicle Model */}
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.model_id} required name="model_id" id="model_id" className="form-select">
                <option value="">Select a Vehicle Model</option>
                {models.map(model => (
                  <option key={model.id} value={model.id}>{model.name}</option>
                ))}
              </select>
            </div>

            {/* Button to submit the form */}
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateAutomobile;

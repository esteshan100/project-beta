import { useEffect, useState } from 'react';

// ShowSales is responsible for displaying a list of sales and providing an option to delete each one.
function ShowSales() {

  // State variable for storing the list of sales fetched from the API.
  const [sales, setSales] = useState([]);

  // Fetch sales data from the API.
  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/sales/');
      if (response.ok) {
        const { sales } = await response.json();
        setSales(sales);
      } else {
        console.error('Server Error:', response);
      }
    } catch (error) {
      console.error('Network Error:', error);
    }
  };

  // Delete a sales by their ID and update the list.
  const deleteCustomer = async (id) => {
    const response = await fetch(`http://localhost:8090/api/sales/${id}`, {
      method: 'DELETE',
    });
    if (response.ok) {
      // Filter out the deleted sales from the state variable.
      setSales(sales.filter((sale) => sale.id !== id));
    }
  };

  // Fetch data initially when the component mounts.
  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Salesperson Employee ID</th>
          <th>Salesperson Name</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Price</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        { // Loop through the list of sales to render each as a row in the table.
          sales.map(sale => (
            <tr key={sale.id}>
              <td>{sale.sales_person.employee_number}</td>
              <td>{sale.sales_person.first_name} {sale.sales_person.last_name}</td>
              <td>{sale.customer.first_name} {sale.customer.last_name}</td>
              <td>{sale.automobile.vin}</td>
              <td>${sale.price}</td>
              <td>
                <button onClick={() => deleteCustomer(sale.id)}>
                  Delete
                </button>
              </td>
            </tr>
          ))
        }
      </tbody>
    </table>
  );
}

export default ShowSales;

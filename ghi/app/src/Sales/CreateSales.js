import React, { useState, useEffect } from 'react';

function CreateSale() {
  // Initialize State for form and dropdowns
  const [salesPersons, setSalesPersons] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [formData, setFormData] = useState({
    price: '',
    automobile: '',
    sales_person: '',
    customer: '',
  });

  // Fetch Salespersons Data from API
  const getSalesPersons = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalesPersons(data.salespeople);
    }
  };

  // Fetch Automobiles Data from API
  const getAutoMobiles = async () => {
    const url = 'http://localhost:8090/api/automobile/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.automobile);
    }
  };

  // Fetch Customer Data from API
  const getCustomers = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };

  // Populate dropdowns when component mounts
  useEffect(() => {
    getCustomers();
    getAutoMobiles();
    getSalesPersons();
  }, []);

  // Handle form submit action
  const handleSubmit = async (event) => {
    event.preventDefault();

    // Configuration for sales API call
    const salesUrl = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    // Post data to Sales API
    const response = await fetch(salesUrl, fetchConfig);

    // Configuration for updating automobile
    const autoUrl = `http://localhost:8100/api/automobiles/${formData.automobile}/`;
    const autoFetchConfig = {
      method: "PUT",
      body: JSON.stringify({"sold": true}),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    // Update automobile as sold
    const automobileResponse = await fetch(autoUrl, autoFetchConfig);

    // Reset the form if both API calls succeed
    if (response.ok && automobileResponse.ok ) {
      setFormData({
        price: '',
        automobile: '',
        sales_person: '',
        customer: '',
      });
    }
  };

  // Update form state on user input
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  };

  // Render form UI
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new sale</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.price} placeholder="price" required type="number" name="price" id="price" className="form-control" />
              <label htmlFor="price">price</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.sales_person} required name="sales_person" id="sales_person" className="form-select">
                <option value="">Choose a sales person</option>
                {salesPersons.map(salesPerson => {
                  return (
                    <option key={salesPerson.id} value={salesPerson.id}>{salesPerson.first_name} {salesPerson.last_name}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.automobile} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose a automobile</option>
                {automobiles.map(automobile => {
                  return (
                    <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.customer} required name="customer" id="customer" className="form-select">
                <option value="">Choose a customer</option>
                {customers.map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateSale;

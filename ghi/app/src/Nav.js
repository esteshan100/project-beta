import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/salesPeople">Show sales people</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/salesPeople/history">Show sales history </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/salesPeople/create">Add Sales person</NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/customers">Show Customers </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/customers/create">Add Customer </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/sales">Show sales </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/sales/create">Add a sale </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/manufacturers/">Manufacturers </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/manufacturers/create"> Add a Manufacturer </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/vehiclemodels"> show vehicle models </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/vehiclemodels/create"> create a vehicle model </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/automobiles"> show automobiles </NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/automobiles/create"> Create automobile </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;

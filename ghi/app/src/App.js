import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AddSalesPerson from './SalesPeople/AddSalesPerson';
import ShowSalesPerson from './SalesPeople/ShowSalesPerson';
import SalesPersonHistory from './SalesPeople/SalesPersonHistory';
import CreateCustomer from './Customers/CreateCustomer';
import ShowCustomers from './Customers/ShowCustomers';
import CreateSales from './Sales/CreateSales';
import ShowSales from './Sales/ShowSales';
import ShowManufacturers from './Manufacturers/ShowManufacturers';
import CreateManufacturer from './Manufacturers/CreateManufacturer';
import ShowVehicleModels from './VehicleModels/ShowVehicleModels';
import CreateVehicleModel from './VehicleModels/CreateVehicleModel';
import ShowAutomobiles from './Automobiles/ShowAutomobiles';
import CreateAutomobile from './Automobiles/CreateAutomobile';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/salesPeople">
            <Route path="" element={<ShowSalesPerson />} />
            <Route path="create" element={<AddSalesPerson />} />
            <Route path="history" element={<SalesPersonHistory />} />
          </Route>
          <Route path="/customers">
            <Route path="create" element={<CreateCustomer />} />
            <Route path="" element={<ShowCustomers />} />
          </Route>
          <Route path="/sales">
            <Route path="create" element={<CreateSales />} />
            <Route path="" element={<ShowSales />} />
          </Route>
          <Route path="/manufacturers">
            <Route path="" element={<ShowManufacturers />} />
            <Route path="create" element={<CreateManufacturer />} />
          </Route>
          <Route path="/vehiclemodels">
            <Route path="" element={<ShowVehicleModels />} />
            <Route path="create" element={<CreateVehicleModel />} />
          </Route>
          <Route path="/automobiles">
            <Route path="" element={<ShowAutomobiles />} />
            <Route path="create" element={<CreateAutomobile />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

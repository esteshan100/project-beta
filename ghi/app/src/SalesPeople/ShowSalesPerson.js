import { useEffect, useState } from 'react';

// Main component for displaying salespeople list
function ListSalesPeople() {
  // State to hold the list of salespeople
  const [salespeople, setSalesPeople] = useState([]);

  // Function to fetch data from the API
  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/salespeople/');
      if (response.ok) {
        const data = await response.json();
        setSalesPeople(data.salespeople);
      } else {
        // Handle server errors
        console.error('Server Error:', response);
      }
    } catch (error) {
      // Handle network errors
      console.error('Network Error:', error);
    }
  };

  // Function to delete a salesperson by their id
  const deleteSalesPerson = async (id) => {
    const response = await fetch(`http://localhost:8090/api/salespeople/${id}`, {
      method: 'DELETE',
    });
    if (response.ok) {
      // Refresh the list if the deletion is successful
      setSalesPeople(salespeople.filter((person) => person.id !== id));
    }
  };

  // Fetch data when the component mounts
  useEffect(() => {
    getData();
  }, []);


  return (
    // Create a table with bootstrap class names for styling
    <table className="table table-striped">
      <thead>
        <tr>
          <th>first name</th>
          <th>last name</th>
          <th>employee number</th>
          <th>remove</th>
        </tr>
      </thead>
      <tbody>
        {
          // Map over the 'salespeople' array to generate rows for each salesperson
          salespeople.map(salesperson => {
            return (
              // Use the 'id' as a key to ensure each row is unique
              <tr key={salesperson.id}>
                {/* Display the first name of the salesperson */}
                <td>{ salesperson.first_name }</td>
                {/* Display the last name of the salesperson */}
                <td>{ salesperson.last_name }</td>
                {/* Display the employee number of the salesperson */}
                <td>{ salesperson.employee_number }</td>
                {/* Create a button for deleting the salesperson
                    When clicked, it calls the 'deleteSalesPerson' function with the salesperson's id as an argument */}
                <td><button onClick={() => deleteSalesPerson(salesperson.id)}>Delete</button></td>
              </tr>
            );
          })
        }
      </tbody>
    </table>
  );
}

export default ListSalesPeople;

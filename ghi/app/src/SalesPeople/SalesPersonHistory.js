// Importing necessary React hooks
import { useEffect, useState } from 'react';

// Main function component for displaying sales history
function SalesPersonHistory() {

  // State variables to hold sales data and list of salespersons
  const [salespeople, setSalesPeople] = useState([]);
  const [salesPersons, setSalesPersons] = useState([]);

  // State for storing form input
  const [formData, setFormData] = useState({
    sales_person: '',
  });

  // Fetch salespersons from API
  const getSalesPersons = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      // Updating state with fetched salespersons data
      setSalesPersons(data.salespeople);
    }
  };

  // Fetch sales data from API
  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/sales/');
      if (response.ok) {
        const data = await response.json();
        // Updating state with fetched sales data
        setSalesPeople(data.sales);
      } else {
        console.error('Server Error:', response);
      }
    } catch (error) {
      console.error('Network Error:', error);
    }
  };

  // Fetching data when the component mounts
  useEffect(() => {
    getData();
    getSalesPersons();
  }, []);

  // Handling form changes
  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    // Update form state based on user input
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div>
      <h1>Salesperson History</h1>

      {/* Dropdown for selecting a salesperson */}
      <div className="mb-3">
        <select onChange={handleFormChange} value={formData.sales_person} required name="sales_person" id="sales_person" className="form-select">
          <option value="">Choose a sales person</option>
          {salesPersons.map(salesPerson => (
            <option key={salesPerson.id} value={salesPerson.id}>{salesPerson.first_name} {salesPerson.last_name}</option>
          ))}
        </select>
      </div>

      {/* Table for displaying sales data */}
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>

        {/* Table Body */}
        <tbody>
          {
            // Filtering and mapping over the sales data
            salespeople
              .filter(sale => sale.sales_person.id.toString() === formData.sales_person) // Filtering by selected salesperson
              .map(sale => (
                // Mapping to create table rows for each filtered sale
                <tr key={sale.id}>
                  <td>{sale.sales_person.first_name} {sale.sales_person.last_name}</td>
                  <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>{sale.price}</td>
                </tr>
              ))
          }
        </tbody>
      </table>
    </div>
  );
}

// Exporting the component for use in other files
export default SalesPersonHistory;

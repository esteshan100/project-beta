import { useEffect, useState } from 'react';

// ShowVehicleModels is responsible for displaying a list of vehicle models.
function ShowVehicleModels() {

  // State variable for storing the list of vehicle models fetched from the API.
  const [models, setVehicleModels] = useState([]);

  // Fetch vehicle models data from the API.
  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/models/');
      if (response.ok) {
        // De-structure models from the API response and set it to state.
        const { models } = await response.json();
        setVehicleModels(models);
      } else {
        console.error('Server Error:', response);
      }
    } catch (error) {
      console.error('Network Error:', error);
    }
  };

  // Trigger the getData function to fetch models when the component mounts.
  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
        <h1>Vehicle Models</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {
              // Loop through the list of vehicle models to render each as a row in the table.
              models.map(model => (
                <tr key={model.id}>
                  <td>{model.name}</td>
                  <td>{model.manufacturer.name}</td>
                  <td><img src={model.picture_url} alt={model.name} width="100" /></td>
                </tr>
              ))
            }
          </tbody>
        </table>
    </div>
  );
}

export default ShowVehicleModels;

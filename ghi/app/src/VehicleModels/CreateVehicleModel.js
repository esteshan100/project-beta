import React, { useState, useEffect } from 'react';

// CreateVehicleModel component is responsible for creating new vehicle models.
function CreateVehicleModel() {

  // State to store the list of manufacturers fetched from the API.
  const [manufacturers, setManufacturers] = useState([]);

  // State to store form data.
  const [formData, setFormData] = useState({
    name: '',
    picture_url: '',
    manufacturer_id: '',
  });

  // Fetch list of manufacturers from the API.
  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/manufacturers/');
      if (response.ok) {
        const { manufacturers } = await response.json();
        setManufacturers(manufacturers);
      } else {
        console.error('Server Error:', response);
      }
    } catch (error) {
      console.error('Network Error:', error);
    }
  };

  // Initial fetch for the list of manufacturers when component mounts.
  useEffect(() => {
    getData();
  }, []);

  // Function to handle form submission.
  const handleSubmit = async (event) => {
    event.preventDefault();

    // URL and configurations for POST request to create a vehicle model.
    const customerUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    // Make POST request with form data.
    const response = await fetch(customerUrl, fetchConfig);

    // If the API call is successful, reset form data to initial state.
    if (response.ok) {
      setFormData({
        name: '',
        picture_url: '',
        manufacturer_id: '',
      });
    }
  }

  // Function to handle changes to form inputs.
  const handleFormChange = (e) => {
    const value = e.target.value;  // Extract value from the changed input.
    const inputName = e.target.name;  // Extract input name.

    // Update formData state to include new input value.
    setFormData({
      ...formData,
      [inputName]: value,
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a vehicle model</h1>
          <form onSubmit={handleSubmit}>
            {/* Input field for Name */}
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>

            {/* Input field for Picture URL */}
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.picture_url} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture URL</label>
            </div>

            {/* Dropdown to select Manufacturer */}
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                <option value="">Add a manufacturer</option>
                {manufacturers.map(manufacturer => (
                  <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                ))}
              </select>
            </div>

            {/* Button to trigger form submission */}
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateVehicleModel;

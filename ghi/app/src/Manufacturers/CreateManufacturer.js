import React, {useState} from 'react';

function CreateManufacturer() {
  // state to store form data
  const [formData, setFormData] = useState({
    name: '',
  })


  // Function to handle form submission
  const handleSubmit = async (event) => {
    event.preventDefault();

    const customerUrl = 'http://localhost:8100/api/manufacturers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    // Make POST request with form data
    const response = await fetch(customerUrl, fetchConfig);


    // IF API call is successful reset form data to initial state
    if (response.ok) {
      setFormData({
        name: '',
      });
    }
  }

// Define a function to handle changes to form inputs
const handleFormChange = (e) => {
    // Extract the value of the input that triggered the event
    const value = e.target.value;

    // Extract the name of the input (used as the key in the formData object)
    const inputName = e.target.name;

    // Update the formData state
    setFormData({
      // Spread the current formData to keep existing field values
      ...formData,

      // Update only the field that has changed
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a manufacturer</h1>
          <form onSubmit={handleSubmit} id="create-salesPerson-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateManufacturer;

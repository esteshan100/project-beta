import { useEffect, useState } from 'react';

// ShowManufacturers is responsible for displaying a list of manufacturers.
function ShowManufacturers() {

  // State variable for storing the list of customers fetched from the API.
  const [manufacturers, setManufacturers] = useState([]);

  // Fetch manufacturers data from the API.
  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/manufacturers/');
      if (response.ok) {
        const { manufacturers } = await response.json();
        setManufacturers(manufacturers);
      } else {
        console.error('Server Error:', response);
      }
    } catch (error) {
      console.error('Network Error:', error);
    }
  };

  // Fetch data initially when the component mounts.
  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
        <h1>Manufacturers</h1>
        <table className="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            { // Loop through the list of manufacturers to render each as a row in the table.
            manufacturers.map(manufacturer => (
                <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
                </tr>
            ))
            }
        </tbody>
        </table>
    </div>
  );
}

export default ShowManufacturers;

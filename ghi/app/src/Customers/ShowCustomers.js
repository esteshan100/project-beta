import { useEffect, useState } from 'react';

// ShowCustomers is responsible for displaying a list of customers and providing an option to delete each one.
function ShowCustomers() {

  // State variable for storing the list of customers fetched from the API.
  const [customers, setCustomers] = useState([]);

  // Fetch customer data from the API.
  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/customers/');
      if (response.ok) {
        const { customers } = await response.json();
        setCustomers(customers);
      } else {
        console.error('Server Error:', response);
      }
    } catch (error) {
      console.error('Network Error:', error);
    }
  };

  // Delete a customer by their ID and update the list.
  const deleteCustomer = async (id) => {
    const response = await fetch(`http://localhost:8090/api/customers/${id}`, {
      method: 'DELETE',
    });
    if (response.ok) {
      // Filter out the deleted customer from the state variable.
      setCustomers(customers.filter((customer) => customer.id !== id));
    }
  };

  // Fetch data initially when the component mounts.
  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>first name</th>
          <th>last name</th>
          <th>address</th>
          <th>phone number</th>
          <th>remove</th>
        </tr>
      </thead>
      <tbody>
        { // Loop through the list of customers to render each as a row in the table.
          customers.map(customer => (
            <tr key={customer.id}>
              <td>{customer.first_name}</td>
              <td>{customer.last_name}</td>
              <td>{customer.address}</td>
              <td>{customer.phone_number}</td>
              <td>
                <button onClick={() => deleteCustomer(customer.id)}>
                  Delete
                </button>
              </td>
            </tr>
          ))
        }
      </tbody>
    </table>
  );
}

export default ShowCustomers;

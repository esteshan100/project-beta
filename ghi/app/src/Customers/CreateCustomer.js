import React, {useState} from 'react';

function CreateCustomer() {
  // state to store form data
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    address: '',
    phone_number: '',
  })


  // Function to handle form submission
  const handleSubmit = async (event) => {
    event.preventDefault();

    const customerUrl = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    // Make POST request with form data
    const response = await fetch(customerUrl, fetchConfig);


    // IF API call is successful reset form data to initial state
    if (response.ok) {
      setFormData({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
      });
    }
  }

// Define a function to handle changes to form inputs
const handleFormChange = (e) => {
    // Extract the value of the input that triggered the event
    const value = e.target.value;

    // Extract the name of the input (used as the key in the formData object)
    const inputName = e.target.name;

    // Update the formData state
    setFormData({
      // Spread the current formData to keep existing field values
      ...formData,

      // Update only the field that has changed
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a customer</h1>
          <form onSubmit={handleSubmit} id="create-salesPerson-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.first_name} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control" />
              <label htmlFor="first_name">first name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.last_name} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control" />
              <label htmlFor="last_name">last name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.address} placeholder="address" required type="text" name="address" id="address" className="form-control" />
              <label htmlFor="address">address</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.phone_number} placeholder="phone_number" required type="number" name="phone_number" id="phone_number" className="form-control" />
              <label htmlFor="phone_number">phone number</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateCustomer;
